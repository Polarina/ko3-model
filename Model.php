<?php defined('SYSPATH') OR die('No direct script access.');

/**
 * Copyright (c) 2013 Gabríel Arthúr Pétursson
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Automated model class.
 *
 * Calls PostgreSQL functions automatically as PHP functions.
 *
 * Known limitations:
 * Does not handle overloaded functions with different return types.
 *
 * @package   Database
 * @category  Controller
 * @author    Gabríel Arthúr Pétursson
 * @copyright (c) 2013 Gabríel Arthúr Pétursson
 */

class Model {

	private static $functions_sql = "
		SELECT
			p.proname function_name,
			p.proretset function_returns_multiple,
			t.typname = 'record' function_returns_record
		FROM
			pg_catalog.pg_proc p
			JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid
			JOIN pg_catalog.pg_type t ON p.prorettype = t.oid
		WHERE
			n.nspname = 'public'
	";

	private $functions = array();
	private $before;
	private $after;

	private function call($name, $arguments)
	{
		$parameters = array();

		foreach ($arguments as $key => $value)
		{
			$parameters[] = $key.' := :'.$key;
		}

		$db = DB::query(Database::SELECT, 'SELECT * FROM '.$name.'('.implode(', ', $parameters).')');

		foreach ($arguments as $key => $value)
		{
			$db->param(':'.$key, $value);
		}

		if ( ! isset($this->functions[$name]))
			throw new Database_Exception("Function $name does not exist.
				No function matches the given name and argument types.");

		$function = $this->functions[$name];

		if ($function['multiple'] && $function['record'])
			return $db->execute()->as_array();

		if ($function['multiple'])
		{
			$res = array_values($db->execute()->as_array());

			return array_map(function ($i) {
				$res = array_values($i);
				return $res[0];
			}, $res);
		}

		if ($function['record'])
		{
			$res = $db->execute()->as_array();
			return $res[0];
		}

		$value = $db->execute()->as_array();
		$value = array_values($value[0]);
		$value = $value[0];

		return $value;
	}

	function __construct($before = NULL, $after = NULL)
	{
		$this->before = $before;
		$this->after = $after;

		$functions = DB::query(Database::SELECT, Model::$functions_sql)->execute();

		foreach ($functions as $value)
		{
			$this->functions[$value['function_name']] = array(
				'name'	   => $value['function_name'],
				'multiple' => $value['function_returns_multiple'],
				'record'   => $value['function_returns_record'],
			);
		}
	}

	public function __call($name, $arguments)
	{
		if ($this->before)
		{
			/* we cannot shorten this to a single line;
			   that causes PHP to segfault */
			$func = $this->before;
			$func($name, $arguments);
		}

		$result = $this->call($name, count($arguments) >= 1 ? $arguments[0] : array());

		if ($this->after)
		{
			$func = $this->after;
			$func($name, $arguments, $result);
		}

		return $result;
	}

	public function transaction($function)
	{
		DB::query(Database::SELECT, 'SET SESSION CHARACTERISTICS AS TRANSACTION ISOLATION LEVEL SERIALIZABLE')->execute();

		while (true)
		{
			DB::query(Database::SELECT, 'BEGIN')->execute();

			try
			{
				$success = $function($this);
				break;
			}
			catch (Database_Exception $e)
			{
				if ($e->getCode() == 40001) /* serialization failure */
				{
					DB::query(Database::SELECT, 'ROLLBACK')->execute();
					continue;
				}

				throw $e;
			}
		}

		DB::query(Database::SELECT, $success ? 'COMMIT' : 'ROLLBACK')->execute();
	}

}
